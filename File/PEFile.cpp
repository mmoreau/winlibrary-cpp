#include <iostream>
#include <Windows.h>
#include <iomanip>
#include <string>

using namespace std;


class FileSystem
{
	public:

		BOOL isFile(LPCSTR file)
		{
			DWORD dwAttr{ GetFileAttributesA(file) };

			return (dwAttr == INVALID_FILE_ATTRIBUTES) ? FALSE : ((dwAttr & FILE_ATTRIBUTE_DIRECTORY) ? FALSE : TRUE);
		}
};


class Memory
{
	public:

		/* .:: Checks if a returned variable is a bad pointer in the virtual address space ::. */
		BOOL isBadPtrInVirtualAddressSpace(PVOID variable)
		{
			MEMORY_BASIC_INFORMATION mbi{ 0 };
			return ((VirtualQuery(variable, &mbi, sizeof(mbi)) ? ((mbi.Protect & (PAGE_GUARD | PAGE_NOACCESS)) ? TRUE : FALSE) : TRUE));
		}
};


class PE
{
	private:

		string path{ "" };
		HANDLE hFile{ INVALID_HANDLE_VALUE };
		LPVOID fileData{ nullptr };

		PIMAGE_DOS_HEADER _PIMAGE_DOS_HEADER{};
		PIMAGE_NT_HEADERS _PIMAGE_NT_HEADERS{};
		PIMAGE_SECTION_HEADER _PIMAGE_SECTION_HEADER{};


		void exports_headers(PIMAGE_EXPORT_DIRECTORY _PIMAGE_EXPORT_DIRECTORY)
		{
			cout << "\tCharacteristics       : " << _PIMAGE_EXPORT_DIRECTORY->Characteristics << endl;
			cout << "\tTimeDateStamp         : " << _PIMAGE_EXPORT_DIRECTORY->TimeDateStamp << endl;
			cout << "\tMajorVersion          : " << _PIMAGE_EXPORT_DIRECTORY->MajorVersion << endl;
			cout << "\tMinorVersion          : " << _PIMAGE_EXPORT_DIRECTORY->MinorVersion << endl;
			cout << "\tName                  : " << _PIMAGE_EXPORT_DIRECTORY->Name << endl;
			cout << "\tBase                  : " << _PIMAGE_EXPORT_DIRECTORY->Base << endl;
			cout << "\tNumberOfFunctions     : " << uppercase << hex << _PIMAGE_EXPORT_DIRECTORY->NumberOfFunctions << " => " << dec << _PIMAGE_EXPORT_DIRECTORY->NumberOfFunctions << endl;
			cout << "\tNumberOfNames         : " << uppercase << hex << _PIMAGE_EXPORT_DIRECTORY->NumberOfNames << " => " << dec << _PIMAGE_EXPORT_DIRECTORY->NumberOfNames << endl;
			cout << "\tAddressOfFunctions    : " << uppercase << hex << _PIMAGE_EXPORT_DIRECTORY->AddressOfFunctions << endl;
			cout << "\tAddressOfNames        : " << uppercase << hex << _PIMAGE_EXPORT_DIRECTORY->AddressOfNames << endl;
			cout << "\tAddressOfNameOrdinals : " << uppercase << hex << _PIMAGE_EXPORT_DIRECTORY->AddressOfNameOrdinals << endl;
		}



		void exports_dll(bool banner=TRUE)
		{
			string file{ PE::getPath() };

			string filename_base{ file.substr(file.find_last_of("/\\") + 1) };
			string filename_ext{ filename_base.substr(filename_base.find_last_of(".") + 1) };

			if (filename_ext == "dll")
			{
				LPCSTR dllName{ (static_cast<LPCSTR>(filename_base.c_str())) };
				PVOID dllBase{ LoadLibraryA(dllName) };

				PVOID address{ static_cast<LPBYTE>(dllBase) + _PIMAGE_DOS_HEADER->e_lfanew };
				PVOID address2 { static_cast<LPBYTE>(dllBase) + _PIMAGE_NT_HEADERS->OptionalHeader.DataDirectory[0].VirtualAddress };

				Memory memory;

				if (!memory.isBadPtrInVirtualAddressSpace(address) && !memory.isBadPtrInVirtualAddressSpace(address2))
				{
					PIMAGE_DOS_HEADER _PIMAGE_DOS_HEADER{ reinterpret_cast<PIMAGE_DOS_HEADER>(dllBase) };
					PIMAGE_NT_HEADERS _PIMAGE_NT_HEADERS{ reinterpret_cast<PIMAGE_NT_HEADERS>(address) };
					PIMAGE_EXPORT_DIRECTORY _PIMAGE_EXPORT_DIRECTORY{ reinterpret_cast<PIMAGE_EXPORT_DIRECTORY>(address2) };

					if (banner)
					{
						PE::exports_headers(_PIMAGE_EXPORT_DIRECTORY);
					}

					if (_PIMAGE_EXPORT_DIRECTORY->Characteristics == 0)
					{
						WORD* pOrdinal{ reinterpret_cast<WORD*>(static_cast<LPBYTE>(dllBase) + _PIMAGE_EXPORT_DIRECTORY->AddressOfNameOrdinals) };
						DWORD* pName{ reinterpret_cast<DWORD*>(static_cast<LPBYTE>(dllBase) + _PIMAGE_EXPORT_DIRECTORY->AddressOfNames) };
						DWORD* pFunction{ reinterpret_cast<DWORD*>(static_cast<LPBYTE>(dllBase) + _PIMAGE_EXPORT_DIRECTORY->AddressOfFunctions) };

						cout << "\n\t" << dllName << " (0x" << hex << dllBase << ")\n" << endl;

						for (int i{ 0 }; i < _PIMAGE_EXPORT_DIRECTORY->NumberOfNames; ++i)
						{
							//DWORD functionRVA{ pFunction[pOrdinal[i]] };
							DWORD nameRVA{ pName[i] };
							LPCSTR functionName{ reinterpret_cast<LPCSTR>(static_cast<LPBYTE>(dllBase) + nameRVA) };
							PVOID functionAddress{ static_cast<LPBYTE>(dllBase) + nameRVA }; // (PVOID) functionName

							//cout << "\t\t - " << hex << functionAddress << " # " << hex << nameRVA << " # " << functionName << endl;

							cout << "\t\t - 0x" << functionAddress << " # " << functionName << endl;
						}
					}
				}
				else
				{
					cout << "\tCannot recover data due to read protection" << endl;
				}

				
			}
		}


	public:

		PE(string file)
		{
			path = file;
			LPCSTR path_lpcstr{ path.c_str() };

			FileSystem fs;

			if (fs.isFile(path_lpcstr))
			{
				hFile = CreateFileA(path_lpcstr, GENERIC_READ, FILE_SHARE_READ, nullptr, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, nullptr);

				DWORD fileSize{ GetFileSize(hFile, nullptr) };
				fileData = HeapAlloc(GetProcessHeap(), 0, fileSize);
				DWORD bytesRead{ 0 };

				if (fileData != nullptr)
				{
					if (ReadFile(hFile, fileData, fileSize, &bytesRead, nullptr))
					{
						_PIMAGE_DOS_HEADER = reinterpret_cast<PIMAGE_DOS_HEADER>(fileData);
						_PIMAGE_NT_HEADERS = reinterpret_cast<PIMAGE_NT_HEADERS>(static_cast<LPBYTE>(fileData) + _PIMAGE_DOS_HEADER->e_lfanew);
						_PIMAGE_SECTION_HEADER = IMAGE_FIRST_SECTION(reinterpret_cast<IMAGE_NT_HEADERS*>(static_cast<LPBYTE>(fileData) + _PIMAGE_DOS_HEADER->e_lfanew));
					}
				}
			}
		}



		~PE()
		{
			if (hFile != INVALID_HANDLE_VALUE)
			{
				CloseHandle(hFile);

				if (fileData)
				{
					HeapFree(GetProcessHeap(), 0, fileData);
				}
			}
		}


		FARPROC GetProcLibrary(LPCSTR moduleName, LPCSTR procName)
		{
			HMODULE hmodule { GetModuleHandleA(moduleName) };

			return (hmodule != 0) ? GetProcAddress(hmodule, procName) : nullptr;
		}



		void dos_header()
		{
			cout << ".:: DOS HEADER ::.\n" << endl;

			if (fileData)
			{
				int magicNumber{ _PIMAGE_DOS_HEADER->e_magic };

				if (magicNumber == 0x5a4d)
				{
					cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << magicNumber << ": Magic Number" << endl;
					cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_DOS_HEADER->e_cblp << ": Bytes on last page of file" << endl;
					cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_DOS_HEADER->e_cp << ": Pages in file" << endl;
					cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_DOS_HEADER->e_crlc << ": Relocations" << endl;
					cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_DOS_HEADER->e_cparhdr << ": Size of header in paragraphs" << endl;
					cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_DOS_HEADER->e_minalloc << ": Minimum extra paragraphs needed" << endl;
					cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_DOS_HEADER->e_maxalloc << ": Maximum extra paragraphs needed" << endl;
					cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_DOS_HEADER->e_ss << ": Initial (relative) SS value" << endl;
					cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_DOS_HEADER->e_sp << ": Initial SP value" << endl;
					cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_DOS_HEADER->e_csum << ": Checksum" << endl;
					cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_DOS_HEADER->e_ip << ": Initial IP value" << endl;
					cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_DOS_HEADER->e_cs << ": Initial (relative) CS value" << endl;
					cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_DOS_HEADER->e_lfarlc << ": File address of relocation table" << endl;
					cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_DOS_HEADER->e_ovno << ": Overlay number" << endl;

					for (int i(0); i < 4; ++i)
					{
						cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_DOS_HEADER->e_res[i] << ": Reserved words [" << i << "]" << endl;
					}

					cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_DOS_HEADER->e_oemid << ": OEM identifier" << endl;
					cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_DOS_HEADER->e_oeminfo << ": OEM information" << endl;

					for (int i(0); i < 10; ++i)
					{
						cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_DOS_HEADER->e_res2[i] << ": Reserved words [" << i << "]" << endl;
					}

					cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_DOS_HEADER->e_lfanew << ": File address of new exe header" << endl;
				}
			}
			else
			{
				cout << "\tNo data loaded in memory" << endl;
			}
		}



		void nt_headers()
		{
			cout << "\n.:: NT HEADERS ::.\n" << endl;

			if (fileData)
			{
				cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_NT_HEADERS->Signature << ": Signature" << endl;
			}
			else
			{
				cout << "\tNo data loaded in memory" << endl;
			}
		}



		void file_header()
		{
			cout << "\n.:: FILE HEADER ::.\n" << endl;

			if (fileData)
			{
				cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_NT_HEADERS->FileHeader.Machine << ": Machine" << endl;
				cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_NT_HEADERS->FileHeader.NumberOfSections << ": Number of Sections" << endl;
				cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_NT_HEADERS->FileHeader.TimeDateStamp << ": Time Stamp" << endl;
				cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_NT_HEADERS->FileHeader.PointerToSymbolTable << ": Pointer to Symbol Table" << endl;
				cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_NT_HEADERS->FileHeader.NumberOfSymbols << ": Number of Symbols" << endl;
				cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_NT_HEADERS->FileHeader.SizeOfOptionalHeader << ": Size of Optional Header" << endl;
				cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_NT_HEADERS->FileHeader.Characteristics << ": Characteristics" << endl;
			}
			else
			{
				cout << "\tNo data loaded in memory" << endl;
			}
		}



		void optional_header()
		{
			cout << "\n.:: OPTIONAL HEADER ::.\n" << endl;

			if (fileData)
			{
				cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_NT_HEADERS->OptionalHeader.Magic << ": Magic" << endl;
				cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << static_cast<int>(static_cast<unsigned char>(_PIMAGE_NT_HEADERS->OptionalHeader.MajorLinkerVersion)) << ": Major Linker Version" << endl;
				cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << static_cast<int>(static_cast<unsigned char>(_PIMAGE_NT_HEADERS->OptionalHeader.MinorLinkerVersion)) << ": Minor Linker Version" << endl;
				cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_NT_HEADERS->OptionalHeader.SizeOfCode << ": Size Of Code" << endl;
				cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_NT_HEADERS->OptionalHeader.SizeOfInitializedData << ": Size Of Initialized Data" << endl;
				cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_NT_HEADERS->OptionalHeader.SizeOfUninitializedData << ": Size Of UnInitialized Data" << endl;
				cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_NT_HEADERS->OptionalHeader.AddressOfEntryPoint << ": Address Of Entry Point" << endl;
				cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_NT_HEADERS->OptionalHeader.BaseOfCode << ": Base Of Code" << endl;
				cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_NT_HEADERS->OptionalHeader.ImageBase << ": Image Base" << endl;
				cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_NT_HEADERS->OptionalHeader.SectionAlignment << ": Section Alignment" << endl;
				cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_NT_HEADERS->OptionalHeader.FileAlignment << ": File Alignment" << endl;
				cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_NT_HEADERS->OptionalHeader.MajorOperatingSystemVersion << ": Major Operating System Version" << endl;
				cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_NT_HEADERS->OptionalHeader.MinorOperatingSystemVersion << ": Minor Operating System Version" << endl;
				cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_NT_HEADERS->OptionalHeader.MajorImageVersion << ": Major Image Version" << endl;
				cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_NT_HEADERS->OptionalHeader.MinorImageVersion << ": Minor Image Version" << endl;
				cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_NT_HEADERS->OptionalHeader.MajorSubsystemVersion << ": Major Subsystem Version" << endl;
				cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_NT_HEADERS->OptionalHeader.MinorSubsystemVersion << ": Minor Subsystem Version" << endl;
				cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_NT_HEADERS->OptionalHeader.Win32VersionValue << ": Win32 Version Value" << endl;
				cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_NT_HEADERS->OptionalHeader.SizeOfImage << ": Size Of Image" << endl;
				cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_NT_HEADERS->OptionalHeader.SizeOfHeaders << ": Size Of Headers" << endl;
				cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_NT_HEADERS->OptionalHeader.CheckSum << ": CheckSum" << endl;
				cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_NT_HEADERS->OptionalHeader.Subsystem << ": Subsystem" << endl;
				cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_NT_HEADERS->OptionalHeader.DllCharacteristics << ": DllCharacteristics" << endl;
				cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_NT_HEADERS->OptionalHeader.SizeOfStackReserve << ": Size Of Stack Reserve" << endl;
				cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_NT_HEADERS->OptionalHeader.SizeOfStackCommit << ": Size Of Stack Commit" << endl;
				cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_NT_HEADERS->OptionalHeader.SizeOfHeapReserve << ": Size Of Heap Reserve" << endl;
				cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_NT_HEADERS->OptionalHeader.SizeOfHeapCommit << ": Size Of Heap Commit" << endl;
				cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_NT_HEADERS->OptionalHeader.LoaderFlags << ": Loader Flags" << endl;
				cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_NT_HEADERS->OptionalHeader.NumberOfRvaAndSizes << ": Number Of Rva And Sizes" << endl;
			}
			else
			{
				cout << "\tNo data loaded in memory" << endl;
			}
		}



		void data_directories()
		{
			cout << "\n.:: DATA DIRECTORIES ::.\n" << endl;

			if (fileData)
			{
				cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_NT_HEADERS->OptionalHeader.DataDirectory[0].VirtualAddress << ": Export Directory RVA" << endl;
				cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_NT_HEADERS->OptionalHeader.DataDirectory[0].Size << ": Export Directory Size" << endl;

				cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_NT_HEADERS->OptionalHeader.DataDirectory[1].VirtualAddress << ": Import Directory RVA" << endl;
				cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_NT_HEADERS->OptionalHeader.DataDirectory[1].Size << ": Import Directory Size" << endl;

				cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_NT_HEADERS->OptionalHeader.DataDirectory[2].VirtualAddress << ": Resource Directory RVA" << endl;
				cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_NT_HEADERS->OptionalHeader.DataDirectory[2].Size << ": Resource Directory Size" << endl;

				cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_NT_HEADERS->OptionalHeader.DataDirectory[3].VirtualAddress << ": Exception Directory RVA" << endl;
				cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_NT_HEADERS->OptionalHeader.DataDirectory[3].Size << ": Exception Directory Size" << endl;

				cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_NT_HEADERS->OptionalHeader.DataDirectory[4].VirtualAddress << ": Security Directory Offset" << endl;
				cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_NT_HEADERS->OptionalHeader.DataDirectory[4].Size << ": Security Directory Size" << endl;

				cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_NT_HEADERS->OptionalHeader.DataDirectory[5].VirtualAddress << ": Relocation Directory RVA" << endl;
				cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_NT_HEADERS->OptionalHeader.DataDirectory[5].Size << ": Relocation Directory Size" << endl;

				cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_NT_HEADERS->OptionalHeader.DataDirectory[6].VirtualAddress << ": Debug Directory RVA" << endl;
				cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_NT_HEADERS->OptionalHeader.DataDirectory[6].Size << ": Debug Directory Size" << endl;

				cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_NT_HEADERS->OptionalHeader.DataDirectory[7].VirtualAddress << ": Architecture Directory RVA" << endl;
				cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_NT_HEADERS->OptionalHeader.DataDirectory[7].Size << ": Architecture Directory Size" << endl;

				cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_NT_HEADERS->OptionalHeader.DataDirectory[8].VirtualAddress << ": Reserved" << endl;
				cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_NT_HEADERS->OptionalHeader.DataDirectory[8].Size << ": Reserved" << endl;

				cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_NT_HEADERS->OptionalHeader.DataDirectory[9].VirtualAddress << ": TLS Directory RVA" << endl;
				cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_NT_HEADERS->OptionalHeader.DataDirectory[9].Size << ": TLS Directory Size" << endl;

				cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_NT_HEADERS->OptionalHeader.DataDirectory[10].VirtualAddress << ": Load Config Directory RVA" << endl;
				cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_NT_HEADERS->OptionalHeader.DataDirectory[10].Size << ": Load Config Directory Size" << endl;

				cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_NT_HEADERS->OptionalHeader.DataDirectory[11].VirtualAddress << ": Bound Import Directory RVA" << endl;
				cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_NT_HEADERS->OptionalHeader.DataDirectory[11].Size << ": Bound Import Directory Size" << endl;

				cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_NT_HEADERS->OptionalHeader.DataDirectory[12].VirtualAddress << ": Import Address Table Directory RVA" << endl;
				cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_NT_HEADERS->OptionalHeader.DataDirectory[12].Size << ": Import Address Table Directory RVA Size" << endl;

				cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_NT_HEADERS->OptionalHeader.DataDirectory[13].VirtualAddress << ": Delay Import Directory RVA" << endl;
				cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_NT_HEADERS->OptionalHeader.DataDirectory[13].Size << ": Delay Import Directory Size" << endl;

				cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_NT_HEADERS->OptionalHeader.DataDirectory[14].VirtualAddress << ": .NET Directory Directory RVA" << endl;
				cout << uppercase << "\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_NT_HEADERS->OptionalHeader.DataDirectory[14].Size << ": .NET Directory Size" << endl;
			}
			else
			{
				cout << "\tNo data loaded in memory" << endl;
			}
		}



		void section_headers()
		{
			cout << "\n .:: SECTION HEADERS ::.\n" << endl;

			if (fileData)
			{
				for (int i(0); i < _PIMAGE_NT_HEADERS->FileHeader.NumberOfSections; ++i) // for (int i (0); i < 8; ++i)
				{
					cout << uppercase << "\t" << _PIMAGE_SECTION_HEADER[i].Name << endl;
					cout << uppercase << "\t\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_SECTION_HEADER[i].Misc.PhysicalAddress << ": Virtual Physical Address" << endl;
					cout << uppercase << "\t\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_SECTION_HEADER[i].Misc.VirtualSize << ": Virtual Size" << endl;
					cout << uppercase << "\t\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_SECTION_HEADER[i].VirtualAddress << ": Virtual Address" << endl;
					cout << uppercase << "\t\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_SECTION_HEADER[i].SizeOfRawData << ": Size Of Raw Data" << endl;
					cout << uppercase << "\t\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_SECTION_HEADER[i].PointerToRawData << ": Pointer To Raw Data" << endl;
					cout << uppercase << "\t\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_SECTION_HEADER[i].PointerToRelocations << ": Pointer To Relocations" << endl;
					cout << uppercase << "\t\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_SECTION_HEADER[i].PointerToLinenumbers << ": Pointer To Line Numbers" << endl;
					cout << uppercase << "\t\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_SECTION_HEADER[i].NumberOfRelocations << ": Number Of Relocations" << endl;
					cout << uppercase << "\t\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_SECTION_HEADER[i].NumberOfLinenumbers << ": Number Of Line Numbers" << endl;
					cout << uppercase << "\t\t0x" << left << setw(14) << setfill(' ') << hex << _PIMAGE_SECTION_HEADER[i].Characteristics << ": Characteristics" << endl;
				}
			}
			else
			{
				cout << "\tNo data loaded in memory" << endl;
			}
		}



		void imports()
		{
			cout << "\n.:: DLL IMPORTS ::.\n" << endl;

			if (fileData)
			{
				if (_PIMAGE_NT_HEADERS->OptionalHeader.DataDirectory[1].Size != 0)
				{
					DWORD offset{ PE::RVA2Offset(_PIMAGE_NT_HEADERS->OptionalHeader.DataDirectory[1].VirtualAddress, _PIMAGE_SECTION_HEADER) };

					PIMAGE_IMPORT_DESCRIPTOR _PIMAGE_IMPORT_DESCRIPTOR{ reinterpret_cast<PIMAGE_IMPORT_DESCRIPTOR>(static_cast<LPBYTE>(fileData) + offset) };

					Memory memory;

					if (!memory.isBadPtrInVirtualAddressSpace(_PIMAGE_IMPORT_DESCRIPTOR))
					{
						while (_PIMAGE_IMPORT_DESCRIPTOR->Name != 0)
						{
							LPBYTE dllName{ static_cast<LPBYTE>(fileData) + PE::RVA2Offset(_PIMAGE_IMPORT_DESCRIPTOR->Name, _PIMAGE_SECTION_HEADER) };
							PVOID dllAddressInMemory{ LoadLibraryA(reinterpret_cast<LPCSTR>(dllName)) };

							if (dllAddressInMemory != nullptr)
							{
								cout << "\t" << dllName << " (0x" << hex << dllAddressInMemory << ")\n" << endl;
							}
							else
							{
								cout << "\t" << dllName << "\n" << endl;
							}


							IMAGE_THUNK_DATA * pINT{ reinterpret_cast<IMAGE_THUNK_DATA*>(static_cast<LPBYTE>(fileData) + PE::RVA2Offset(_PIMAGE_IMPORT_DESCRIPTOR->OriginalFirstThunk, _PIMAGE_SECTION_HEADER)) };
							//IMAGE_THUNK_DATA* pIAT{ reinterpret_cast<IMAGE_THUNK_DATA*>(static_cast<LPBYTE>(fileData) + PE::RVA2Offset(pImportDescriptor->FirstThunk, pSectionHeaders)) };

							if (pINT->u1.AddressOfData < 0x80000000)
							{
								while (pINT->u1.Function)
								{
									IMAGE_IMPORT_BY_NAME* pFuncName{ reinterpret_cast<IMAGE_IMPORT_BY_NAME*>(static_cast<LPBYTE>(fileData) + PE::RVA2Offset(static_cast<DWORD>(pINT->u1.AddressOfData), _PIMAGE_SECTION_HEADER)) };

									/*
									cout << "\t\t" << left << setw(14) << setfill(' ') << hex << pINT->u1.AddressOfData << ": AddressOfData (OriginalFirstThunk)" << endl;
									cout << "\t\t" << left << setw(14) << setfill(' ') << hex << pIAT->u1.AddressOfData << ": AddressOfData (FirstThunk)" << endl;
									cout << "\t\t" << left << setw(14) << setfill(' ') << hex << pFuncName->Hint << ": Hint" << endl;
									cout << "\t\t" << left << setw(13) << setfill(' ') << pFuncName->Name << " : Name\n" << endl;
									*/

									if (!memory.isBadPtrInVirtualAddressSpace(pFuncName))
									{
										// Retrieves the address of the module's function from memory
										FARPROC funcNameAddressInMemory{ PE::GetProcLibrary(reinterpret_cast<LPCSTR>(dllName), pFuncName->Name) };

										if (funcNameAddressInMemory != nullptr)
										{
											cout << "\t\t- 0x" << funcNameAddressInMemory << " # " << pFuncName->Name << endl;
										}
										else
										{
											cout << "\t\t- " << pFuncName->Name << endl;
										}
									}

									++pINT;
									//++pIAT;
								}

								cout << endl;
							}

							cout << "\t---------------------------------------\n" << endl;

							++_PIMAGE_IMPORT_DESCRIPTOR;
						}
					}
					else
					{
						cout << "\tCannot recover data due to read protection" << endl;
					}
				}
				else
				{
					cout << "\tNo DLL Imports" << endl;
				}
			}
			else
			{
				cout << "\tNo data loaded in memory" << endl;
			}
		}



		void exports(bool banner=TRUE)
		{
			cout << "\n.:: DLL EXPORTS ::.\n" << endl;

			if (fileData)
			{
				if (_PIMAGE_NT_HEADERS->OptionalHeader.DataDirectory[0].Size != 0)
				{
					DWORD offset{ RVA2Offset(_PIMAGE_NT_HEADERS->OptionalHeader.DataDirectory[0].VirtualAddress, _PIMAGE_SECTION_HEADER) };

					PIMAGE_EXPORT_DIRECTORY _PIMAGE_EXPORT_DIRECTORY{ reinterpret_cast<IMAGE_EXPORT_DIRECTORY*>(static_cast<LPBYTE>(fileData) + offset) };
					Memory memory;

					if (!memory.isBadPtrInVirtualAddressSpace(_PIMAGE_EXPORT_DIRECTORY))
					{
						string file { PE::getPath() };

						string filename_base{ file.substr(file.find_last_of("/\\") + 1) };
						string filename_ext{ filename_base.substr(filename_base.find_last_of(".") + 1) };

						if (filename_ext == "exe")
						{
							if (banner)
							{
								PE::exports_headers(_PIMAGE_EXPORT_DIRECTORY);
							}
						}

						if (_PIMAGE_EXPORT_DIRECTORY->Characteristics == 0)
						{
							if (_PIMAGE_EXPORT_DIRECTORY->NumberOfFunctions < 0x80000000)
							{
								PIMAGE_IMPORT_DESCRIPTOR _PIMAGE_IMPORT_DESCRIPTOR { reinterpret_cast<PIMAGE_IMPORT_DESCRIPTOR>(static_cast<LPBYTE>(fileData) + offset) };

								WORD* pOrdinalTable{ reinterpret_cast<WORD*>(static_cast<LPBYTE>(fileData) + PE::RVA2Offset(_PIMAGE_EXPORT_DIRECTORY->AddressOfNameOrdinals, _PIMAGE_SECTION_HEADER)) };
								DWORD* pNameTable{ reinterpret_cast<DWORD*>(static_cast<LPBYTE>(fileData) + PE::RVA2Offset(_PIMAGE_EXPORT_DIRECTORY->AddressOfNames, _PIMAGE_SECTION_HEADER)) };
								DWORD* dwAddresses{ reinterpret_cast<DWORD*>(static_cast<LPBYTE>(fileData) + PE::RVA2Offset(_PIMAGE_EXPORT_DIRECTORY->AddressOfFunctions, _PIMAGE_SECTION_HEADER)) };

								LPBYTE dllName{ static_cast<LPBYTE>(fileData) + PE::RVA2Offset(_PIMAGE_IMPORT_DESCRIPTOR->Name, _PIMAGE_SECTION_HEADER) };
								PVOID dllAddressInMemory{ LoadLibraryA(reinterpret_cast<LPCSTR>(dllName)) };

								if (dllAddressInMemory != nullptr)
								{
									cout << "\t" << dllName << " (0x" << hex << dllAddressInMemory << ")\n" << endl;
								}
								else
								{
									cout << "\t" << dllName << "\n" << endl;
								}

								for (int i{0}; i < _PIMAGE_EXPORT_DIRECTORY->NumberOfNames; ++i)
								{
									//DWORD functionRVA { dwAddresses[pOrdinalTable[i]] };
									DWORD nameRVA{ pNameTable[i] };
									LPCSTR functionName{ reinterpret_cast<LPCSTR>(static_cast<LPBYTE>(fileData) + PE::RVA2Offset(nameRVA, _PIMAGE_SECTION_HEADER)) };

									//cout << "\t\t - " << functionRVA << " # " << nameRVA << " # " << functionName << endl;
									cout << "\t\t - " << functionName << endl;
								}
							}
							else
							{
								cout << "\tNo DLL Exports" << endl;
							}
						}
						else
						{
							PE::exports_dll(banner);
						}
					}
					else
					{
						cout << "\tCannot recover data due to read protection" << endl;
					}
				}
				else
				{
					cout << "\tNo DLL Export" << endl;
				}
			}
			else
			{
				cout << "\tNo data loaded in memory" << endl;
			}
		}



		/* .:: Convert Virtual Address to File Offset ::. */
		DWORD RVA2Offset(DWORD rva, PIMAGE_SECTION_HEADER _PIMAGE_SECTION_HEADER_TMP)
		{
			++_PIMAGE_SECTION_HEADER_TMP;
			return (rva == 0) ? rva : (rva - _PIMAGE_SECTION_HEADER_TMP->VirtualAddress + _PIMAGE_SECTION_HEADER_TMP->PointerToRawData);
		}



		string getPath()
		{
			return path;
		}
};

int main()
{
	system("color A");

	/*
		- "C:\\Windows\\System32\\crypt32.dll"
		- "C:\\Windows\\System32\\ntdll.dll"
		- "C:\\Windows\\System32\\cmd.exe"
	*/

	string path{ "C:\\Windows\\System32\\ntdll.dll" };

	PE* pe { new PE(path) };

	cout << pe->getPath() << "\n" << endl;

	pe->dos_header();
	pe->nt_headers();
	pe->file_header();
	pe->optional_header();
	pe->data_directories();
	pe->section_headers();
	pe->imports();
	pe->exports();

	delete pe;

	system("pause");
	return 0;
}