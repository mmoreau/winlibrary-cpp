#include <iostream>
#include <Windows.h>
#include <Psapi.h>
#include <winternl.h>

#pragma comment(lib,"ntdll.lib")

using namespace std;

typedef struct _RTL_PROCESS_MODULE_INFORMATION
{
	HANDLE Section;
	PVOID MappedBase;
	PVOID ImageBase;
	ULONG ImageSize;
	ULONG Flags;
	USHORT LoadOrderIndex;
	USHORT InitOrderIndex;
	USHORT LoadCount;
	USHORT OffsetToFileName;
	UCHAR FullPathName[256];
} RTL_PROCESS_MODULE_INFORMATION, * PRTL_PROCESS_MODULE_INFORMATION;

typedef struct _RTL_PROCESS_MODULES
{
	ULONG NumberOfModules;
	RTL_PROCESS_MODULE_INFORMATION Modules[1];
} RTL_PROCESS_MODULES, * PRTL_PROCESS_MODULES;


class Driver
{
	public:


		/* .:: List the drivers installed on the system ::. */
		void list()
		{
			LPVOID drivers[1024];
			DWORD cbNeeded{ 0 };

			if (EnumDeviceDrivers(drivers, sizeof(drivers), &cbNeeded))
			{
				unsigned long drivers_count{ cbNeeded / sizeof(drivers[0]) };

				for (int i(0); i < drivers_count; ++i)
				{
					cout << "\n\t---------------------- # [" << (i + 1) << " / " << drivers_count << "] # ----------------------\n" << endl;

					char* filename{ new char[256]() };

					if (GetDeviceDriverBaseNameA(drivers[i], filename, 256))
					{
						wcout << "\tFilename : " << filename << endl;

						char* path { new char[1024]() };

						if(GetDeviceDriverFileNameA(drivers[i], path, 1024))
						{
							wcout << "\tPath : " << path << endl;
						}
						
						wcout << endl;

						path = nullptr;
						delete[] path;
					}

					filename = nullptr;
					delete[] filename;
				}
			}
		}



		/* .:: List the drivers installed on the system ::.
			Note : Drivers listed by NtQuerySystemInformation can be hidden with DKOM
		*/
		void list2()
		{
			PRTL_PROCESS_MODULES ModuleInfo{ (PRTL_PROCESS_MODULES)VirtualAlloc(nullptr, 1048576, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE) };

			if (ModuleInfo)
			{
				NTSTATUS status{ NtQuerySystemInformation((SYSTEM_INFORMATION_CLASS)11, ModuleInfo, 1048576, nullptr) };

				if (NT_SUCCESS(status))
				{
					ULONG NumberOfModules{ ModuleInfo->NumberOfModules };

					for (int i(0); i < NumberOfModules; ++i)
					{
						cout << "\n\t---------------------- # [" << (i + 1) << " / " << NumberOfModules << "] # ----------------------\n" << endl;

						cout << "\tImage Base : 0x" << ModuleInfo->Modules[i].ImageBase << endl;
						cout << "\tImage Name : " << ModuleInfo->Modules[i].FullPathName + ModuleInfo->Modules[i].OffsetToFileName << endl;
						cout << "\tImage Full Path : " << ModuleInfo->Modules[i].FullPathName << endl;
						cout << "\tImage Size : " << ModuleInfo->Modules[i].ImageSize << endl;
					}
				}

				VirtualFree(ModuleInfo, 0, MEM_RELEASE);
			}
		}
};

int main()
{
	Driver d;

	d.list();
	//d.list2();

	return 0;
}