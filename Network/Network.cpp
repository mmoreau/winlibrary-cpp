#include <iostream>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <iphlpapi.h>
#include <Windows.h>
#include <vector>
#include <string>

#pragma comment(lib, "iphlpapi.lib")
#pragma comment(lib, "ws2_32.lib")

#define MALLOC(x) HeapAlloc(GetProcessHeap(), 0, (x))
#define FREE(x) HeapFree(GetProcessHeap(), 0, (x))

using namespace std;

string state2name(unsigned long dwState)
{
	string dwStateName{ "" };

	switch (dwState)
	{
		case MIB_TCP_STATE_CLOSED:
			dwStateName = "CLOSED";
			break;
		case MIB_TCP_STATE_LISTEN: // 2
			dwStateName = "LISTEN";
			break;
		case MIB_TCP_STATE_SYN_SENT:
			dwStateName = "SYN-SENT";
			break;
		case MIB_TCP_STATE_SYN_RCVD:
			dwStateName = "SYN-RECEIVED";
			break;
		case MIB_TCP_STATE_ESTAB: // 5
			dwStateName = "ESTABLISHED";
			break;
		case MIB_TCP_STATE_FIN_WAIT1:
			dwStateName = "FIN-WAIT-1";
			break;
		case MIB_TCP_STATE_FIN_WAIT2:
			dwStateName = "FIN-WAIT-2";
			break;
		case MIB_TCP_STATE_CLOSE_WAIT:
			dwStateName = "CLOSE-WAIT";
			break;
		case MIB_TCP_STATE_CLOSING:
			dwStateName = "CLOSING";
			break;
		case MIB_TCP_STATE_LAST_ACK:
			dwStateName = "LAST-ACK";
			break;
		case MIB_TCP_STATE_TIME_WAIT: // 11
			dwStateName = "TIME-WAIT";
			break;
		case MIB_TCP_STATE_DELETE_TCB:
			dwStateName = "DELETE-TCB";
			break;
		default:
			dwStateName = "UNKNOWN";
			break;
	}

	return dwStateName;
}

string addr2ipv4(unsigned long addr)
{
	string addrs{ "" };

	addrs.append(to_string(addr & 0xFF));
	addrs.append(".");
	addrs.append(to_string(addr >> 8 & 0xFF));
	addrs.append(".");
	addrs.append(to_string(addr >> 16 & 0xFF));
	addrs.append(".");
	addrs.append(to_string(addr >> 24 & 0xFF));

	return addrs;
}

void tcp4(int pid)
{
	cout << "PID : " << pid << endl;
	cout << "\n\tPID # LOCAL # REMOTE # STATE\n" << endl;

	vector<unsigned char> buffer;
	DWORD dwSize = sizeof(MIB_TCPTABLE_OWNER_PID); // 28 ?
	DWORD dwRetValue = 0;

	do
	{
		buffer.resize(dwSize, 0);
		dwRetValue = GetExtendedTcpTable(buffer.data(), &dwSize, TRUE, AF_INET, TCP_TABLE_OWNER_PID_ALL, 0);
	} while (dwRetValue == ERROR_INSUFFICIENT_BUFFER);

	if (dwRetValue == ERROR_SUCCESS)
	{
		PMIB_TCPTABLE_OWNER_PID ptTable = reinterpret_cast<PMIB_TCPTABLE_OWNER_PID>(buffer.data());

		for (int i(0); i < ptTable->dwNumEntries; ++i)
		{
			if (ptTable->table[i].dwOwningPid == pid) 
			{
				cout << "\t" << ptTable->table[i].dwOwningPid
					<< " # " << addr2ipv4(ptTable->table[i].dwLocalAddr) << ":" << ptTable->table[i].dwLocalPort
					<< " # " << addr2ipv4(ptTable->table[i].dwRemoteAddr) << ":" << ptTable->table[i].dwRemotePort
					<< " # " << ptTable->table[i].dwState << " => " << state2name(ptTable->table[i].dwState) << endl;
			}
		}
	}
}


void tcp4_v2()
{
	PMIB_TCPTABLE2 pTcpTable { (MIB_TCPTABLE2 *) MALLOC(sizeof(MIB_TCPTABLE2)) };

	if (pTcpTable != NULL) 
	{
		ULONG ulSize = sizeof(MIB_TCPTABLE);

		if (GetTcpTable2(pTcpTable, &ulSize, TRUE) == ERROR_INSUFFICIENT_BUFFER)
		{
			FREE(pTcpTable);
			pTcpTable = (MIB_TCPTABLE2 *) MALLOC(ulSize);

			if (pTcpTable != NULL) {

				if (GetTcpTable2(pTcpTable, &ulSize, TRUE) == NO_ERROR) {

					cout << "TOTAL CONNECTION : " << pTcpTable->dwNumEntries << endl;
					cout << "\n\tPID # LOCAL # REMOTE # STATE\n" << endl;

					for (int i(0); i < pTcpTable->dwNumEntries; ++i)
					{
						wchar_t ipstringbuffer[32];

						cout << "\t" << pTcpTable->table[i].dwOwningPid;
						wcout << " # " << InetNtopW(AF_INET, &pTcpTable->table[i].dwLocalAddr, ipstringbuffer, 32);
						cout << ":" << pTcpTable->table[i].dwLocalPort;
						wcout << " # " << InetNtopW(AF_INET, &pTcpTable->table[i].dwRemoteAddr, ipstringbuffer, 32);
						cout << ":" << pTcpTable->table[i].dwRemotePort;
						cout << " # " << pTcpTable->table[i].dwState << " => " << state2name(pTcpTable->table[i].dwState) << endl;					
					}
				}
			}
		}
	}

	FREE(pTcpTable);
	pTcpTable = nullptr;
}


void tcp6()
{
	PMIB_TCP6TABLE2 pTcpTable{ (MIB_TCP6TABLE2 *) MALLOC(sizeof(MIB_TCP6TABLE2)) };

	if (pTcpTable != NULL)
	{
		ULONG ulSize = sizeof(MIB_TCP6TABLE);

		if (GetTcp6Table2(pTcpTable, &ulSize, TRUE) == ERROR_INSUFFICIENT_BUFFER)
		{
			FREE(pTcpTable);
			pTcpTable = (MIB_TCP6TABLE2 *) MALLOC(ulSize);

			if (pTcpTable != NULL) {

				if (GetTcp6Table2(pTcpTable, &ulSize, TRUE) == NO_ERROR) 
				{
					cout << "TOTAL CONNECTION : " << pTcpTable->dwNumEntries << endl;
					cout << "\n\tPID # LOCAL # REMOTE # STATE\n" << endl;

					for (int i(0); i < pTcpTable->dwNumEntries; ++i)
					{
						wchar_t ipstringbuffer[80];
						
						cout << "\t" << pTcpTable->table[i].dwOwningPid;
						wcout << " # " << InetNtopW(AF_INET6, &pTcpTable->table[i].LocalAddr, ipstringbuffer, 80);
						cout << ":" << pTcpTable->table[i].dwLocalPort;
						wcout << " # " << InetNtopW(AF_INET6, &pTcpTable->table[i].RemoteAddr, ipstringbuffer, 80);
						cout << ":" << pTcpTable->table[i].dwRemotePort;
						cout << " # " << pTcpTable->table[i].State << " => " << state2name(pTcpTable->table[i].State) << endl;
					}
				}
			}
		}
	}

	FREE(pTcpTable);
	pTcpTable = nullptr;
}

int main()
{
	//tcp4(8620);
	//tcp4_v2();
	tcp6();
	
	system("pause");
	return 0;
}