#include <iostream>
#include <windows.h>
#include <string>
#include <array>

using namespace std;

wstring DriveLetter2FileSystemName(LPCWSTR lpRootPathName)
{
	WCHAR fileSystemName[261] = L"";
	GetVolumeInformation(lpRootPathName, NULL, NULL, NULL, NULL, NULL, fileSystemName, sizeof(fileSystemName));
	return fileSystemName;
}


wstring DriveLetter2Name(LPCWSTR lpRootPathName)
{
	WCHAR volumeName[261] = L"";
	GetVolumeInformation(lpRootPathName, volumeName, sizeof(volumeName), NULL, NULL, NULL, NULL, NULL);
	return volumeName;
}


string DriveLetter2Type(LPCWSTR lpRootPathName)
{
	array<string, 6>Value{"NO_ROOT_DIR", "REMOVABLE", "FIXED", "REMOTE", "CDROM", "RAMDISK"};
	UINT DriveType = GetDriveType(lpRootPathName);
	return ((DriveType > 0 && DriveType <= 6) ? Value[(DriveType - 1)] : "UNKNOWN");
}


LPWSTR DriveLetter2DeviceName(LPCWSTR lpDeviceName)
{
	LPWSTR lpTargetPath((LPWSTR) new BYTE[100]);
	QueryDosDeviceW(lpDeviceName, lpTargetPath, 100);
	return lpTargetPath;
}


LPWCH Guid2DriveLetter(LPCWSTR lpszVolumeName)
{
	int length(wcslen(lpszVolumeName));
	LPWCH lpszVolumePathNames((LPWCH) new BYTE[length]);
	DWORD cchBufferLength((DWORD) length);
	PDWORD lpcchReturnLength(NULL);

	GetVolumePathNamesForVolumeNameW(lpszVolumeName, lpszVolumePathNames, cchBufferLength, lpcchReturnLength);

	return lpszVolumePathNames;
}


void GetVolumes()
{
	PWCHAR DriveLetter;
	WCHAR DeviceName[260] = L"";
	WCHAR VolumeName[260] = L"";
	HANDLE FindHandle = FindFirstVolumeW(VolumeName, 260);

	if (FindHandle != INVALID_HANDLE_VALUE)
	{
		while (FindNextVolumeW(FindHandle, VolumeName, 260))
		{
			VolumeName[48] = L'\0';	

			if (QueryDosDevice(&VolumeName[4], DeviceName, 260) > 0)
			{
				VolumeName[48] = L'\\';

				DriveLetter = Guid2DriveLetter(VolumeName);

				if (wcslen(DriveLetter) == 3)
				{
					wcout << "DeviceName  : " << DeviceName << endl;
					wcout << "VolumeName  : " << VolumeName << endl;
					wcout << "DriveLetter : " << DriveLetter << "\n" << endl;
				}	
			}
		}
	}

	FindVolumeClose(FindHandle);
}

void GetDriveLetter()
{
	WCHAR Drive[3] = L"A:";
	DWORD DriveBitMask = GetLogicalDrives();

	while (DriveBitMask)
	{
		if (DriveBitMask & 1)
		{
			wcout << Drive << endl;
		}

		++Drive[0];
		DriveBitMask >>= 1;
	}
}

int main()
{
	//wcout << DriveLetter2DeviceName(L"E:") << endl;
	//GetVolumes();
	//wcout << Guid2DriveLetter(L"\\\\?\\Volume{f4b4a60c-64e1-4123-a78f-9aa136d53063}\\") << endl;
	GetDriveLetter();
	//cout << DriveLetter2Type(L"C:") << endl;
	//wcout << "Volume Name : " << DriveLetter2Name(L"E:") << endl;
	//wcout << "Volume File System Name : " << DriveLetter2FileSystemName(L"E:") << endl;

	system("pause");
	return 0;
}