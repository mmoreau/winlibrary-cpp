#include "header.h"

class DateTime
{
	public:

		string now()
		{
			struct tm newtime;
			time_t now{ time(nullptr) };
			localtime_s(&newtime, &now);

			char datetime[15];

			sprintf_s(
				datetime,
				"%04d%02d%02d%02d%02d%02d",
				newtime.tm_year + 1900,
				newtime.tm_mon + 1,
				newtime.tm_mday,
				newtime.tm_hour,
				newtime.tm_min,
				newtime.tm_sec
			);

			return datetime;
		}
};



class Util
{
	public:

		string UserCurrent()
		{
			TCHAR username[64]{ "" };
			DWORD username_len{ 64 };

			GetUserName(username, &username_len);

			return username;

		}


		string Sid2User(PSID sid)
		{
			TCHAR Name[260]{ "" };
			DWORD cchName{ 260 };
			TCHAR ReferencedDomainName[260];
			DWORD cchReferencedDomainName{ 260 };

			SID_NAME_USE peUse { SidTypeComputer };

			LookupAccountSid(nullptr, sid, Name, &cchName, ReferencedDomainName, &cchReferencedDomainName, &peUse);

			return Name;
		}
};



class String
{
	public:

		/* .:: Converts unicode characters into a string ::. */
		string wstr2str(const wstring& wstr)
		{
			string str(wstr.begin(), wstr.end());

			return str;
		}
};



class Network
{
	public:

		/* .:: Retrieves the name from the state ::. */
		string state2name(const unsigned long state)
		{
			array<string, 12>states{ "CLOSED", "LISTEN", "SYN-SENT", "SYN-RCVD", "ESTABLISHED", "FIN-WAIT-1", "FIN-WAIT-2", "CLOSE-WAIT", "CLOSING", "LAST-ACK", "TIME-WAIT", "DELETE-TCB" };
			return ((state > 0 && state <= 12) ? states[(state - 1)] : "UNKNOWN");
		}



		/* .:: Lists the network connections of a process in TCP IPV4 ::. */
		void tcp(int pid)
		{
			PMIB_TCPTABLE2 tcp4table{ (MIB_TCPTABLE2*)MALLOC(sizeof(MIB_TCPTABLE2)) };

			if (tcp4table != nullptr)
			{
				ULONG ulSize{ sizeof(MIB_TCPTABLE) };

				if (GetTcpTable2(tcp4table, &ulSize, TRUE) == ERROR_INSUFFICIENT_BUFFER)
				{
					FREE(tcp4table);
					tcp4table = (MIB_TCPTABLE2*)MALLOC(ulSize);

					if (tcp4table != nullptr)
					{
						if (GetTcpTable2(tcp4table, &ulSize, TRUE) == NO_ERROR)
						{
							for (int i(0); i < tcp4table->dwNumEntries; ++i)
							{
								if (tcp4table->table[i].dwOwningPid == pid)
								{
									wchar_t ipstringbuffer[32];

									cout << "\t" << tcp4table->table[i].dwOwningPid;
									wcout << " # " << InetNtopW(AF_INET, &tcp4table->table[i].dwLocalAddr, ipstringbuffer, 32);
									cout << ":" << tcp4table->table[i].dwLocalPort;
									wcout << " # " << InetNtopW(AF_INET, &tcp4table->table[i].dwRemoteAddr, ipstringbuffer, 32);
									cout << ":" << tcp4table->table[i].dwRemotePort;
									cout << " # " << Network::state2name(tcp4table->table[i].dwState) << endl;
								}
							}
						}
					}
				}
			}

			FREE(tcp4table);
			tcp4table = nullptr;
		}



		/* .:: Lists the network connections of a process in TCP IPV6 ::. */
		void tcp6(int pid)
		{
			PMIB_TCP6TABLE2 tcp6table{ (MIB_TCP6TABLE2*)MALLOC(sizeof(MIB_TCP6TABLE2)) };

			if (tcp6table != nullptr)
			{
				ULONG ulSize{ sizeof(MIB_TCP6TABLE) };

				if (GetTcp6Table2(tcp6table, &ulSize, TRUE) == ERROR_INSUFFICIENT_BUFFER)
				{
					FREE(tcp6table);
					tcp6table = (MIB_TCP6TABLE2*)MALLOC(ulSize);

					if (tcp6table != nullptr)
					{
						if (GetTcp6Table2(tcp6table, &ulSize, TRUE) == NO_ERROR)
						{
							for (int i(0); i < tcp6table->dwNumEntries; ++i)
							{
								if (tcp6table->table[i].dwOwningPid == pid)
								{
									wchar_t ipstringbuffer[80];

									cout << "\t" << tcp6table->table[i].dwOwningPid;
									wcout << " # " << InetNtopW(AF_INET6, &tcp6table->table[i].LocalAddr, ipstringbuffer, 80);
									cout << ":" << tcp6table->table[i].dwLocalPort;
									wcout << " # " << InetNtopW(AF_INET6, &tcp6table->table[i].RemoteAddr, ipstringbuffer, 80);
									cout << ":" << tcp6table->table[i].dwRemotePort;
									cout << " # " << Network::state2name(tcp6table->table[i].State) << endl;
								}
							}
						}
					}
				}
			}

			FREE(tcp6table);
			tcp6table = nullptr;
		}
};



class Process
{
	public:

		FARPROC GetProcLibrary(LPCSTR moduleName, LPCSTR procName)
		{
			HMODULE hmodule{ GetModuleHandleA(moduleName) };

			if (hmodule != 0)
			{
				return GetProcAddress(hmodule, procName);
			}
		}



		/* .:: Retrieves the name of the process via it's ID ::. */
		string pid2name(DWORD pid)
		{
			string path{ Process::path(pid) };
			return (!path.empty()) ? path.substr(path.find_last_of("/\\") + 1) : path;
		}



		/* .:: Lists all process identifiers ::. */
		vector<int> name2pids(wstring processName)
		{
			//wcout << processName << "\n" << endl;
			//cout << "\t PPID # PID\n" << endl;

			vector<int> pids {};

			HANDLE snapprocess{ CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0) };

			if (snapprocess != INVALID_HANDLE_VALUE)
			{
				PROCESSENTRY32W p32;
				p32.dwSize = sizeof(p32);

				while (Process32NextW(snapprocess, &p32))
				{
					if (wstring(p32.szExeFile) == processName)
					{
						//cout << "\t" << p32.th32ParentProcessID << " # " << p32.th32ProcessID << endl;
						pids.push_back(p32.th32ProcessID);
					}
				}
			}

			CloseHandle(snapprocess);

			return pids;
		}



		/* .:: Lists all processes ::. */
		void list()
		{
			HANDLE snapprocess{ CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0) };

			if (snapprocess != INVALID_HANDLE_VALUE)
			{
				PROCESSENTRY32W p32;
				p32.dwSize = sizeof(p32);

				while(Process32NextW(snapprocess, &p32))
				{
					wcout << "[" << p32.th32ParentProcessID << " / " << p32.th32ProcessID << "] " << p32.szExeFile << endl;
				}
			}

			CloseHandle(snapprocess);
		}


		/* .:: Another way to list all processes ::.
		   Note : If you are an administrator you will be able to retrieve the names of other users, if not your own.
		*/
		void list2()
		{
			WTS_PROCESS_INFO* wpi{ nullptr };
			DWORD count{ 0 };

			if (WTSEnumerateProcesses(WTS_CURRENT_SERVER_HANDLE, 0, 1, &wpi, &count))
			{
				Util util;

				for (DWORD i(0); i < count; ++i)
				{
					cout << "[" << wpi[i].ProcessId << "] " << wpi[i].pProcessName << " # " << wpi[i].SessionId << " & " << util.Sid2User(wpi[i].pUserSid) << endl;
				}
			}

			WTSFreeMemory(wpi);
		}


		/* .:: Lists the DLLs that have been loaded by a process ::. */
		void modules(DWORD pid)
		{
			HANDLE hProcess{ OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, pid) };

			if (hProcess != INVALID_HANDLE_VALUE)
			{
				HMODULE hMods[1024];
				DWORD cbNeeded;

				if (EnumProcessModules(hProcess, hMods, sizeof(hMods), &cbNeeded))
				{
					for (int i(0); i < (cbNeeded / 8); ++i)
					{
						TCHAR szModName[260];

						if (GetModuleFileNameEx(hProcess, hMods[i], szModName, 260))
						{
							cout << szModName << endl; // hMods[i]
						}
					}
				}
			}

			CloseHandle(hProcess);
		}


		/* .:: Another way to recover DLLs loaded by a process ::.
		   Note : This function displays better than the other function, but the other function is not bad.
		*/
		void modules_v2(DWORD pid)
		{
			HANDLE hModuleSnap{ CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, pid) };

			if (hModuleSnap != INVALID_HANDLE_VALUE)
			{
				MODULEENTRY32 me32;
				me32.dwSize = sizeof(MODULEENTRY32);

				while (Module32Next(hModuleSnap, &me32))
				{
					//wcout << "Module Name : " << me32.szModule << endl;
					cout << "Executable : " << me32.szExePath << endl;
					/*
					cout << "Process ID : " << me32.th32ProcessID << endl;
					cout << "Ref count (g) : " << me32.GlblcntUsage << endl;
					cout << "Ref count (p) : " << me32.ProccntUsage << endl;
					cout << "Base Address : " << (DWORD)me32.modBaseAddr << endl;
					cout << "Base Size : " << me32.modBaseSize << endl;
					cout << "------------------------------------------\n" << endl;
					*/
				}
			}

			CloseHandle(hModuleSnap);
		}


		/* .:: Path where the program is launched ::. */
		string path(DWORD pid)
		{
			HANDLE hProcess{ OpenProcess(PROCESS_QUERY_INFORMATION, FALSE, pid) };

			if (hProcess != INVALID_HANDLE_VALUE)
			{
				DWORD value { 260 };
				char * buffer { new char[260]() };
				QueryFullProcessImageNameA(hProcess, 0, buffer, &value);

				CloseHandle(hProcess);

				return buffer;
			}
			else
			{
				CloseHandle(hProcess);
				return "";
			}
		}


		/* .:: Address in the memory where the Process Environment Block (PEB) of a process is located ::. */
		PVOID peb(DWORD pid)
		{
			HANDLE hProcess{ OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, pid) };
			PVOID pebBaseAddress{ nullptr };

			if (hProcess != INVALID_HANDLE_VALUE)
			{
				_NtQueryInformationProcess NtQueryInformationProcess{ (_NtQueryInformationProcess)Process::GetProcLibrary("ntdll.dll", "NtQueryInformationProcess") };
				PROCESS_BASIC_INFORMATION pbi;

				NtQueryInformationProcess(hProcess, 0, &pbi, 48, nullptr); // 48 <=> sizeof(pbi)

				pebBaseAddress = pbi.PebBaseAddress;
			}

			CloseHandle(hProcess);

			return pebBaseAddress;
		}



		// .:: Lists the threads of a process ::.
		void threads(DWORD pid)
		{
			HANDLE snapthread{ CreateToolhelp32Snapshot(TH32CS_SNAPTHREAD, 0) };

			if (snapthread != INVALID_HANDLE_VALUE)
			{
				cout << pid << endl;

				THREADENTRY32 te32;
				te32.dwSize = sizeof(te32);

				while (Thread32Next(snapthread, &te32))
				{
					if (pid == te32.th32OwnerProcessID)
					{
						cout << "\t" << te32.th32ThreadID << endl;
					}
				}
			}

			CloseHandle(snapthread);
		}


		// .:: [User-Mode] # Retrieves the command line of a process ::.
		string cmd(DWORD pid)
		{
			HANDLE hProcess{ OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, pid) };
			string data { "" };

			if (hProcess != INVALID_HANDLE_VALUE)
			{
				PBYTE peb{ (PBYTE)new BYTE[0x28] };

				if (ReadProcessMemory(hProcess, Process::peb(pid), peb, 0x28, nullptr))
				{
					PBYTE* parameters{ (PBYTE*) * (LPVOID*)(peb + 0x20) };
					PBYTE processParameters{ (PBYTE) new BYTE[0x80] };

					if (ReadProcessMemory(hProcess, parameters, processParameters, 0x80, nullptr))
					{
						UNICODE_STRING* pCommandLine{ (UNICODE_STRING*)(processParameters + 0x70) };
						USHORT maximumLength{ pCommandLine->MaximumLength };
						PWSTR cmdLine{ (PWSTR) new BYTE[maximumLength] };

						if (ReadProcessMemory(hProcess, pCommandLine->Buffer, cmdLine, maximumLength, nullptr))
						{
							String str;
							data = str.wstr2str(cmdLine);
						}

						delete[] cmdLine;
					}

					delete[] processParameters;
				}

				delete[] peb;
			}

			CloseHandle(hProcess);

			return data;
		}



		/* .:: [User-Mode] # Retrieve the current path of the directory of a Process ::. */
		string directory(DWORD pid)
		{
			HANDLE hProcess{ OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, pid) };
			string data { "" };

			if (hProcess != INVALID_HANDLE_VALUE)
			{
				PBYTE peb{ (PBYTE)new BYTE[0x28] };

				if (ReadProcessMemory(hProcess, Process::peb(pid), peb, 0x28, nullptr))
				{
					PBYTE* parameters{ (PBYTE*) * (LPVOID*)(peb + 0x20) };
					PBYTE processParameters{ (PBYTE) new BYTE[0x80] };

					if (ReadProcessMemory(hProcess, parameters, processParameters, 0x80, nullptr))
					{
						UNICODE_STRING* pCurrentDirectoryPath{ (UNICODE_STRING*)(processParameters + 0x38) };
						USHORT maximumLength{ pCurrentDirectoryPath->MaximumLength };
						PWSTR currentDirectoryPath{ (PWSTR) new BYTE[maximumLength] };

						if (ReadProcessMemory(hProcess, pCurrentDirectoryPath->Buffer, currentDirectoryPath, maximumLength, nullptr))
						{
							String str;
							data = str.wstr2str(currentDirectoryPath);
						}

						delete[] currentDirectoryPath;
					}

					delete[] processParameters;
				}

				delete[] peb;
			}

			CloseHandle(hProcess);

			return data;
		}



		/* .:: [User-Mod] # Another way to retrieve the path where the executable was launched ::. */
		string imagePathName(DWORD pid)
		{
			HANDLE hProcess{ OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, pid) };
			string data { "" };

			if (hProcess != INVALID_HANDLE_VALUE)
			{
				PBYTE peb{ (PBYTE)new BYTE[0x28] };

				if (ReadProcessMemory(hProcess, Process::peb(pid), peb, 0x28, nullptr))
				{
					PBYTE* parameters{ (PBYTE*) * (LPVOID*)(peb + 0x20) };
					PBYTE processParameters{ (PBYTE) new BYTE[0x80] };

					if (ReadProcessMemory(hProcess, parameters, processParameters, 0x80, nullptr))
					{
						UNICODE_STRING* pimagePathName{ (UNICODE_STRING*)(processParameters + 0x60) };
						USHORT maximumLength{ pimagePathName->MaximumLength };
						PWSTR imagePathName{ (PWSTR) new BYTE[maximumLength] };

						if (ReadProcessMemory(hProcess, pimagePathName->Buffer, imagePathName, maximumLength, nullptr))
						{
							String str;
							data = str.wstr2str(imagePathName);
						}

						delete[] imagePathName;
					}

					delete[] processParameters;
				}

				delete[] peb;
			}

			CloseHandle(hProcess);

			return data;
		}



		/* .:: Lists the network connections of a process in TCP IPV4 & TCP IPV6 ::. */
		void connection(int pid)
		{			
			cout << "\n\tPID # LOCAL # REMOTE # STATE\n" << endl;

			Network network;

			network.tcp(pid);
			network.tcp6(pid);
		}



		/* .:: Retrieves the date and time of a process start ::.
		   Note : Beware, this is not always true and can be modified by the process or another process.
		*/
		string startTime(DWORD pid)
		{
			HANDLE hProcess = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, pid);

			if (hProcess != INVALID_HANDLE_VALUE)
			{
				FILETIME lpCreationTime;
				FILETIME lpExitTime;
				FILETIME lpKernelTime;
				FILETIME lpUserTime;

				if (GetProcessTimes(hProcess, &lpCreationTime, &lpExitTime, &lpKernelTime, &lpUserTime))
				{
					FILETIME lpLocalFileTime;

					if (FileTimeToLocalFileTime(&lpCreationTime, &lpLocalFileTime))
					{
						SYSTEMTIME SystemTime;

						if (FileTimeToSystemTime(&lpLocalFileTime, &SystemTime))
						{
							char startTime[20];

							sprintf_s(
								startTime,
								"%04d/%02d/%02d %02d:%02d:%02d",
								SystemTime.wYear,
								SystemTime.wMonth,
								SystemTime.wDay,
								SystemTime.wHour,
								SystemTime.wMinute,
								SystemTime.wSecond
							);

							return startTime;
						}
					}
				}
			}

			return "";
		}



		/* .:: Dump of a process into a DMP file only in User-Mode ::.
		   Note : Analyzing the DMP file with WinDbg
		   Link : https://docs.microsoft.com/en-us/windows/win32/api/minidumpapiset/ne-minidumpapiset-minidump_type
		*/
		bool dump(DWORD pid)
		{
			HANDLE hProcess{ OpenProcess(PROCESS_ALL_ACCESS, FALSE, pid) };
			BOOL isDump{ FALSE };

			if (hProcess != INVALID_HANDLE_VALUE)
			{
				DateTime dt;
				string processName{ Process::pid2name(pid) };
				processName.erase(processName.end() - 4, processName.end());
				string filename_string{ processName + "-" + to_string(pid) + "-" + dt.now() + ".dmp" };
				LPCSTR filename_lpcstr{ filename_string.c_str() };

				HANDLE hFile{ CreateFileA(filename_lpcstr, GENERIC_ALL, 0, nullptr, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, nullptr) };

				if (hFile != INVALID_HANDLE_VALUE)
				{
					constexpr DWORD Flags{
						MiniDumpWithDataSegs |
						MiniDumpWithFullMemory |
						MiniDumpWithHandleData |
						MiniDumpWithUnloadedModules |
						MiniDumpWithIndirectlyReferencedMemory |
						MiniDumpWithProcessThreadData |
						MiniDumpWithPrivateReadWriteMemory |
						MiniDumpWithFullMemoryInfo |
						MiniDumpWithThreadInfo |
						MiniDumpWithCodeSegs |
						MiniDumpWithFullAuxiliaryState |
						MiniDumpWithPrivateWriteCopyMemory |
						MiniDumpWithTokenInformation |
						MiniDumpWithModuleHeaders |
						MiniDumpWithAvxXStateContext |
						MiniDumpWithIptTrace
					};

					isDump = MiniDumpWriteDump(hProcess, pid, hFile, (MINIDUMP_TYPE)Flags, nullptr, nullptr, nullptr);
				}

				CloseHandle(hFile);

				struct stat stat_buf;
				stat(filename_lpcstr, &stat_buf);

				if (stat_buf.st_size <= 0)
				{
					remove(filename_lpcstr);
					isDump = FALSE;
				}
			}

			CloseHandle(hProcess);
			return isDump;
		}



		/* .:: List the processes that belong to the current user ::. */
		void EnumProcess_UserCurrent() 
		{
			WTS_PROCESS_INFO* wpi{ nullptr };
			DWORD count { 0 };

			if (WTSEnumerateProcesses(WTS_CURRENT_SERVER_HANDLE, 0, 1, &wpi, &count))
			{
				Util util;
				string user_current { util.UserCurrent() };
				string user_process;

				for (DWORD i(0); i < count; ++i)
				{
					user_process = util.Sid2User(wpi[i].pUserSid);

					if (user_current.compare(user_process) == 0)
					{
						cout << "\t" << Process::startTime(wpi[i].ProcessId) << " # [" << user_process << " | " << wpi[i].ProcessId << "] " << wpi[i].pProcessName << endl;
					}
				}
			}

			WTSFreeMemory(wpi);
		}
};


class Handle
{
	private:
		unordered_map<int, unordered_map<string, vector <string>>> handles {};

	public:

		/* .:: [User-Mods] # Retrieve all the handles of the running processes ::. */
		void dump()
		{
			Process p;

			_NtQuerySystemInformation NtQuerySystemInformation{ (_NtQuerySystemInformation)p.GetProcLibrary("ntdll.dll", "NtQuerySystemInformation") };
			_NtQueryObject NtQueryObject{ (_NtQueryObject)p.GetProcLibrary("ntdll.dll", "NtQueryObject") };

			ULONG handleInfoSize{ 0x1000 };
			PSYSTEM_HANDLE_INFORMATION handleInfo{ (PSYSTEM_HANDLE_INFORMATION)new BYTE[handleInfoSize]() };
			NTSTATUS status{ NtQuerySystemInformation(SystemHandleInformation, handleInfo, handleInfoSize, nullptr) };

			while (status == STATUS_INFO_LENGTH_MISMATCH)
			{
				handleInfoSize *= 2;
				handleInfo = (PSYSTEM_HANDLE_INFORMATION)realloc(handleInfo, handleInfoSize);
				status = NtQuerySystemInformation(SystemHandleInformation, handleInfo, handleInfoSize, nullptr);
			}

			if (NT_SUCCESS(status))
			{
				ULONG handleCount{ 0 };
				String str;

				while (handleCount < handleInfo->HandleCount)
				{
					SYSTEM_HANDLE handle{ handleInfo->Handles[handleCount] };

					//cout << handle.GrantedAccess << endl;

					if (handle.GrantedAccess != 0x120189 && handle.GrantedAccess != 0x12019f && handle.GrantedAccess != 0x1a019f)
					{
						HANDLE processHandle{ OpenProcess(PROCESS_DUP_HANDLE, FALSE, handle.ProcessId) };

						if (processHandle != INVALID_HANDLE_VALUE)
						{
							HANDLE dupHandle{ nullptr };

							if (DuplicateHandle(processHandle, (HANDLE)handle.Handle, GetCurrentProcess(), &dupHandle, 0, TRUE, DUPLICATE_SAME_ACCESS))
							{
								ULONG ntqueryobject_size{ 0 };

								NtQueryObject(dupHandle, ObjectTypeInformation, nullptr, 0, &ntqueryobject_size);

								POBJECT_TYPE_INFORMATION objectTypeInfo{ (POBJECT_TYPE_INFORMATION) new BYTE[ntqueryobject_size]() };

								if (NT_SUCCESS(NtQueryObject(dupHandle, ObjectTypeInformation, objectTypeInfo, ntqueryobject_size, nullptr)))
								{
									ntqueryobject_size = 0;

									NtQueryObject(dupHandle, ObjectNameInformation, nullptr, 0, &ntqueryobject_size);

									PUNICODE_STRING nameInfo{ (PUNICODE_STRING) new BYTE[ntqueryobject_size] };

									if (NT_SUCCESS(NtQueryObject(dupHandle, ObjectNameInformation, nameInfo, ntqueryobject_size, nullptr)))
									{
										if (nameInfo->Length > 0)
										{
											string buffer{ str.wstr2str(nameInfo->Buffer) };

											if (GetFileType(dupHandle) == FILE_TYPE_DISK)
											{
												if (buffer.substr(0, 22) == "\\Device\\HarddiskVolume")
												{
													char* handle_path{ new char[ntqueryobject_size]() };
													GetFinalPathNameByHandleA(dupHandle, handle_path, ntqueryobject_size, VOLUME_NAME_DOS);

													buffer = handle_path;
													buffer = buffer.erase(0, 4);

													handle_path = nullptr;
													delete[] handle_path;
												}
											}
											
											handles[handle.ProcessId][str.wstr2str(objectTypeInfo->Name.Buffer)].push_back(buffer);

											/*
											cout << "["<< handleCount << " / " << handleInfo->HandleCount << "] @ ";
											wcout << GetFileType(dupHandle) <<  " [" << handle.ProcessId << "] " << objectTypeInfo->Name.Buffer <<  " # " << nameInfo->Buffer << endl;
											*/
										}
									}

									nameInfo = nullptr;
									delete[] nameInfo;
								}

								objectTypeInfo = nullptr;
								delete[] objectTypeInfo;
							}

							CloseHandle(dupHandle);
						}

						CloseHandle(processHandle);
					}

					++handleCount;
				}
			}

			delete handleInfo;
		}



		void enumerate_all(string object_type)
		{
			Process p;

			for (const auto& iter : Handle::handles)
			{
				cout << iter.first << " # " << p.pid2name(iter.first) << "\n" << endl;

				for (const auto& n : Handle::handles[iter.first][object_type])
				{
					cout << "\t" << n << endl;
				}

				cout << endl;
			}
		}



		void show_object_type_pid(int pid, string indent="")
		{
			for (const auto& value : Handle::handles[pid])
			{
				cout << indent << value.first << endl;
			}
		}



		void show_object_type_data_pid(int pid, string type, string indent="")
		{
			for (const auto value : Handle::handles[pid][type])
			{
				cout << indent << value << endl;
			}
		}



		void inspect_by_name(wstring processName, vector<string>object_type)
		{
			Process p;

			for (const auto for_value : p.name2pids(processName))
			{
				cout << for_value << endl;

				for (const auto for_type : object_type)
				{
					cout << "\t" << for_type << endl;
					Handle::show_object_type_data_pid(for_value, for_type, "\t\t");
				}

				cout << endl;
			}
		}
};


int main()
{
	system("color A");

	Process p;

	//cout << p.pid2name(3044) << endl;
	//p.name2pids(L"firefox.exe");
	//p.list();
	//p.list2();
	//p.modules(33552);
	//p.modules_v2(33552);
	//p.path(21984);
	//cout << p.peb(5240) << endl;
	//p.threads(3172);
	//cout << p.cmd(2328) << "\n" << endl;
	//cout << p.directory(2328) << "\n" << endl;
	//cout << p.imagePathName(2328) << "\n" << endl;
	//p.connection(21464);
	//cout << p.startTime(2968) << endl;
	//cout << p.dump(4172) << endl;
	//p.EnumProcess_UserCurrent();

	Handle h;

	h.dump();
	h.enumerate_all("File"); // h.enumerate_all("Key");
	//h.show_object_type_pid(2904);
	//h.show_object_type_data_pid(2904, "Key");
	//h.inspect_by_name(L"firefox.exe", { "Key", "File" });

	system("pause");
	return 0;
}