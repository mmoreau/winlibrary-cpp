#include <iostream>
#include <Windows.h>
#include <LM.h>
#include <Sddl.h>

#pragma comment(lib, "netapi32.lib")
#pragma comment(lib, "Advapi32.lib")

/*
typedef enum _SID_NAME_USE {
	SidTypeUser,
	SidTypeGroup,
	SidTypeDomain,
	SidTypeAlias,
	SidTypeWellKnownGroup,
	SidTypeDeletedAccount,
	SidTypeInvalid,
	SidTypeUnknown,
	SidTypeComputer,
	SidTypeLabel,
	SidTypeLogonSession
} SID_NAME_USE, * PSID_NAME_USE;
*/

using namespace std;

class Group
{
	public:

		BOOL IsUserCurrentAdmin()
		{
			BOOL state { FALSE };
			SID_IDENTIFIER_AUTHORITY NtAuthority { SECURITY_NT_AUTHORITY };
			PSID AdministratorsGroup { nullptr };

			if (AllocateAndInitializeSid(&NtAuthority, 2, SECURITY_BUILTIN_DOMAIN_RID, DOMAIN_ALIAS_RID_ADMINS, 0, 0, 0, 0, 0, 0, &AdministratorsGroup))
			{
				CheckTokenMembership(nullptr, AdministratorsGroup, &state);
			}

			FreeSid(AdministratorsGroup);
			return state;
		}


		void User2LocalGroups(LPCWSTR user)
		{
			wcout << user << endl;

			LPBYTE buffer;
			DWORD entries;
			DWORD total_entries;

			NetUserGetLocalGroups(nullptr, user, 0, LG_INCLUDE_INDIRECT, &buffer, MAX_PREFERRED_LENGTH, &entries, &total_entries);

			LOCALGROUP_USERS_INFO_0 * groups = (LOCALGROUP_USERS_INFO_0 *) buffer;

			for (int i(0); i < entries; ++i)
			{
				wcout << "\t" << groups[i].lgrui0_name << endl;
				//wcout << groups[i].lgrui0_name << " # " << Group::Group2Sid(groups[i].lgrui0_name) << endl;
			}

			NetApiBufferFree(buffer);
		}


		void User2Groups(LPCWSTR user)
		{
			wcout << user << endl;

			LPBYTE buffer;
			DWORD entries;
			DWORD total_entries;

			NetUserGetGroups(nullptr, user, 0, &buffer, MAX_PREFERRED_LENGTH, &entries, &total_entries);

			GROUP_USERS_INFO_0 * ggroups = (GROUP_USERS_INFO_0 *) buffer;

			for (int i(0); i < entries; ++i)
			{
				wcout << "\t" << ggroups[i].grui0_name << endl;
			}

			NetApiBufferFree(buffer);
		}


		wstring Group2Sid(LPCTSTR group)
		{
			TCHAR domain[260];
			BYTE accountSIDbuf[68]; // 68 <=> SECURITY_MAX_SID_SIZE
			PSID accountSID{ (PSID) accountSIDbuf };
			DWORD cbSid{ 68 };
			DWORD cchRD{ 260 };
			SID_NAME_USE snu { SidTypeGroup };
			wstring sid{ L"" };

			if (LookupAccountName(nullptr, group, accountSID, &cbSid, domain, &cchRD, &snu))
			{
				LPTSTR sidstr{ nullptr };

				if (ConvertSidToStringSid(accountSID, &sidstr))
				{
					//wcout << domain << " # " << group << " # " << sidstr << endl;
					sid = (wstring) sidstr;
					LocalFree(sidstr);
				}
			}

			return sid;
		}
};


class User
{
	public: 

		wstring User2Sid(LPCTSTR user)
		{
			TCHAR domain[260];
			BYTE accountSIDbuf[68]; // 68 <=> SECURITY_MAX_SID_SIZE
			PSID accountSID { (PSID) accountSIDbuf };
			DWORD cbSid { 68 };
			DWORD cchRD { 260 };
			SID_NAME_USE snu { SidTypeUser };
			wstring sid { L"" };

			if (LookupAccountName(nullptr, user, accountSID, &cbSid, domain, &cchRD, &snu))
			{
				LPTSTR sidstr { nullptr };

				if (ConvertSidToStringSid(accountSID, &sidstr))
				{
					//wcout << domain << " # " << user << " # " << sidstr << endl;
					sid = (wstring) sidstr;
					LocalFree(sidstr);
				}
			}

			return sid;
		}


		wstring Sid2UserOrGroup(LPCTSTR sid)
		{
			PSID psid { nullptr };

			if (ConvertStringSidToSid(sid, &psid))
			{
				TCHAR Name[260];
				DWORD cchName { 260 };
				TCHAR ReferencedDomainName[260];
				DWORD cchReferencedDomainName { 260 };
				SID_NAME_USE peUse { SidTypeComputer };

				if (LookupAccountSid(nullptr, psid, Name, &cchName, ReferencedDomainName, &cchReferencedDomainName, &peUse))
				{
					return (wstring) Name;
				}
			}

			return L"";
		}

		void List()
		{
			LPUSER_INFO_0 pBuf{ nullptr };
			LPUSER_INFO_0 pTmpBuf{ nullptr };

			DWORD dwEntriesRead { 0 };
			DWORD dwTotalEntries { 0 };
			DWORD dwResumeHandle { 0 };

			NET_API_STATUS nStatus{ NetUserEnum(nullptr, 0, FILTER_NORMAL_ACCOUNT, (LPBYTE *) &pBuf, MAX_PREFERRED_LENGTH, &dwEntriesRead, &dwTotalEntries, &dwResumeHandle) };

			if ((nStatus == NERR_Success) || (nStatus == ERROR_MORE_DATA))
			{
				if ((pTmpBuf = pBuf) != nullptr)
				{
					for (int i(0); i < dwEntriesRead; ++i)
					{
						//wcout << pTmpBuf->usri0_name << " # " << User::User2Sid(pTmpBuf->usri0_name) << endl;
						wcout << pTmpBuf->usri0_name << endl;

						pTmpBuf++;
					}
				}
			}

			NetApiBufferFree(pBuf);
			pBuf = nullptr;
		}
		

		wstring UserCurrent()
		{

			TCHAR username[64];
			DWORD username_len { 64 };

			if (GetUserName(username, &username_len)) 
			{
				return (wstring) username;
			}

			return L"";
		}
};

void LocalComputerName()
{
	TCHAR szComputer[256];
	DWORD dwBufferSize { 512 };

	GetComputerName(szComputer, &dwBufferSize);

	wcout << szComputer << endl;
}

int main()
{
	Group g;

	//cout << g.IsUserCurrentAdmin() << endl;
	//g.User2LocalGroups(L"maxime");
	//g.User2Groups(L"maxime");
	//wcout << g.Group2Sid(L"Utilisateurs") << endl;

	//LocalComputerName();

	User u;

	//wcout << u.User2Sid(L"Maxime") << endl; // u.User2Sid((u.UserCurrent()).c_str())
	u.List();
	//wcout << u.UserCurrent() << endl;
	//wcout << u.Sid2UserOrGroup((u.User2Sid((u.UserCurrent()).c_str()).c_str())) << endl; // wcout << u.Sid2UserOrGroup(L"S-1-5-32-544") << endl;

	system("pause");
	return 0;
}