#include <iostream>
#include <windows.h>
#include <winspool.h>

#pragma comment(lib, "winspool.lib")

using namespace std;

class Printer
{
	public:

		void defaults()
		{
			char szDefaultPrinter[256];
			DWORD dwSize{ 256 };

			cout << (GetDefaultPrinterA(szDefaultPrinter, &dwSize) ? szDefaultPrinter : "No default printer") << endl;
		}


		void list()
		{
			LPBYTE pPrinterEnum{ 0 };
			DWORD bytesNeeded{ 0 };
			DWORD numPrinters{ 0 };

			if (!EnumPrinters(PRINTER_ENUM_LOCAL | PRINTER_ENUM_CONNECTIONS, nullptr, 2, pPrinterEnum, 0, &bytesNeeded, &numPrinters))
			{
				pPrinterEnum = new byte[bytesNeeded];

				if (EnumPrinters(PRINTER_ENUM_LOCAL | PRINTER_ENUM_CONNECTIONS, nullptr, 2, pPrinterEnum, bytesNeeded, &bytesNeeded, &numPrinters))
				{
					PRINTER_INFO_2* pInfo{ (PRINTER_INFO_2*)pPrinterEnum };

					for (int i(0); i < numPrinters; ++i)
					{
						wcout << "PrinterName : " << pInfo->pPrinterName << endl;
						wcout << "PortName : " << pInfo->pPortName << endl;
						wcout << "DriverName : " << pInfo->pDriverName << endl;
						wcout << "Location : " << pInfo->pLocation << endl;
						wcout << "ShareName : " << pInfo->pShareName << endl;
						cout << "Status : " << pInfo->Status << endl;

						cout << "\n------------------------\n" << endl;

						++pInfo;
					}
				}

				pPrinterEnum = nullptr;
				delete[] pPrinterEnum;
			}
		}
};

int main()
{
	SetConsoleOutputCP(1252);

	Printer p;

	//p.defaults();
	p.list();

	return 0;
}
