#include <iostream>
#include <Windows.h>
#include <locale.h>

using namespace std;

class Service
{
	public:

		string state(DWORD dwCurrentState)
		{
			switch (dwCurrentState)
			{
				case SERVICE_STOPPED:
					return"SERVICE_STOPPED";

				case SERVICE_START_PENDING:
					return "SERVICE_START_PENDING";

				case SERVICE_STOP_PENDING:
					return "SERVICE_STOP_PENDING";

				case SERVICE_RUNNING:
					return "SERVICE_RUNNING";

				case SERVICE_CONTINUE_PENDING:
					return "SERVICE_CONTINUE_PENDING";

				case SERVICE_PAUSE_PENDING:
					return "SERVICE_PAUSE_PENDING";

				case SERVICE_PAUSED:
					return "SERVICE_PAUSED";

				default:
					return "UNKNOWN";
			}
		}



		string type(DWORD dwServiceType)
		{
			switch (dwServiceType)
			{
				case SERVICE_FILE_SYSTEM_DRIVER:
					return "SERVICE_FILE_SYSTEM_DRIVER";

				case SERVICE_KERNEL_DRIVER:
					return "SERVICE_KERNEL_DRIVER";

				case SERVICE_WIN32_OWN_PROCESS:
					return "SERVICE_WIN32_OWN_PROCESS";

				case SERVICE_WIN32_SHARE_PROCESS:
					return "SERVICE_WIN32_SHARE_PROCESS";

				case SERVICE_INTERACTIVE_PROCESS:
					return "SERVICE_INTERACTIVE_PROCESS";

				default:
					return "UNKNOWN";
			}
		}



		void list(DWORD service_type = SERVICE_WIN32 | SERVICE_DRIVER)
		{
			SC_HANDLE hSCM{ OpenSCManagerW(nullptr, nullptr, SC_MANAGER_ENUMERATE_SERVICE) };

			DWORD dwBytesNeeded{ 0 };
			DWORD dwServicesReturned{ 0 };

			if (!EnumServicesStatusW(hSCM, service_type, SERVICE_STATE_ALL, nullptr, 0, &dwBytesNeeded, &dwServicesReturned, 0))
			{
				LPENUM_SERVICE_STATUS lpEnumServiceStatus{ (LPENUM_SERVICE_STATUS)LocalAlloc(LPTR, dwBytesNeeded) };

				if (lpEnumServiceStatus != nullptr)
				{
					if (EnumServicesStatusW(hSCM, service_type, SERVICE_STATE_ALL, lpEnumServiceStatus, dwBytesNeeded, &dwBytesNeeded, &dwServicesReturned, 0))
					{
						for (DWORD i(0); i < dwServicesReturned; ++i)
						{
							wcout << "DisplayName : " << lpEnumServiceStatus[i].lpDisplayName << endl;
							wcout << "ServiceName: " << lpEnumServiceStatus[i].lpServiceName << endl;
							cout << "CurrentState : " << Service::state(lpEnumServiceStatus[i].ServiceStatus.dwCurrentState) << endl;
							cout << "ServiceType : " << Service::type(lpEnumServiceStatus[i].ServiceStatus.dwServiceType) << endl;

							cout << "\n------------------------------------\n" << endl;
						}
					}
				}

				LocalFree(lpEnumServiceStatus);
			}

			CloseServiceHandle(hSCM);
		}




		void list2(DWORD service_type = SERVICE_WIN32 | SERVICE_DRIVER)
		{
			SC_HANDLE hScm { OpenSCManagerW(0, SERVICES_ACTIVE_DATABASE, SC_MANAGER_ENUMERATE_SERVICE) };

			if (hScm != nullptr) 
			{
				DWORD ServicesBufferRequired { 0 };
				DWORD ResumeHandle { 0 };
				DWORD ServicesCount { 0 };

				if (!EnumServicesStatusExW(hScm, SC_ENUM_PROCESS_INFO, service_type, SERVICE_STATE_ALL, 0, 0, &ServicesBufferRequired, &ServicesCount, &ResumeHandle, 0))
				{
					ENUM_SERVICE_STATUS_PROCESS* ServicesBuffer{ (ENUM_SERVICE_STATUS_PROCESS*) new char[ServicesBufferRequired] };

					if (EnumServicesStatusExW(hScm, SC_ENUM_PROCESS_INFO, service_type, SERVICE_STATE_ALL, (LPBYTE)ServicesBuffer, ServicesBufferRequired, &ServicesBufferRequired, &ServicesCount, &ResumeHandle, 0))
					{
						for (int i(0); i < ServicesCount; ++i)
						{
							cout << "\tProcess ID : " << ServicesBuffer->ServiceStatusProcess.dwProcessId << "\n" << endl;

							wcout << "\t" << "DisplayName : " << ServicesBuffer->lpDisplayName << endl;
							wcout << "\t" << "ServiceName : " << ServicesBuffer->lpServiceName << "\n" << endl;

							cout << "\t" << "ServiceType : " << ServicesBuffer->ServiceStatusProcess.dwServiceType << " => " << Service::type(ServicesBuffer->ServiceStatusProcess.dwServiceType ) << endl;
							cout << "\t" << "CurrentState : " << ServicesBuffer->ServiceStatusProcess.dwCurrentState << " => " << Service::state(ServicesBuffer->ServiceStatusProcess.dwCurrentState) << endl;
							cout << "\t" << "ControlsAccepted : " << ServicesBuffer->ServiceStatusProcess.dwControlsAccepted << "\n" << endl;
							cout << "\t" << "Win32ExitCode : " << ServicesBuffer->ServiceStatusProcess.dwWin32ExitCode << endl;
							cout << "\t" << "ServiceSpecificExitCode : " << ServicesBuffer->ServiceStatusProcess.dwServiceSpecificExitCode << endl;
							cout << "\t" << "CheckPoint : " << ServicesBuffer->ServiceStatusProcess.dwCheckPoint << endl;
							cout << "\t" << "WaitHint : " << ServicesBuffer->ServiceStatusProcess.dwWaitHint << endl;
							cout << "\t" << "ServiceFlags : " << ServicesBuffer->ServiceStatusProcess.dwServiceFlags << endl;

							cout << "\n\t----------------------------------------------\n" << endl;

							++ServicesBuffer;
						}
					}

					ServicesBuffer = nullptr;
					delete[] ServicesBuffer;
				}

				CloseServiceHandle(hScm);
			}
		}
};


int main()
{
	setlocale(LC_ALL, setlocale(LC_CTYPE, ""));

	Service s;

	//s.list(); // s.list(SERVICE_DRIVER)
	s.list2();

	return 0;
}