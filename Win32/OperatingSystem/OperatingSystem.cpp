#include <iostream>
#include <Windows.h>
#include <string>
#include <chrono>
#include <time.h>

using namespace std;

class DateTime
{
	public:
		
		string display(time_t time)
		{
			struct tm newtime;
			localtime_s(&newtime, &time);

			char datetime[21]{ "" };

			sprintf_s(
				datetime,
				"%04d-%02d-%02d %02d:%02d:%02d",
				newtime.tm_year + 1900,
				newtime.tm_mon + 1,
				newtime.tm_mday,
				newtime.tm_hour,
				newtime.tm_min,
				newtime.tm_sec
			);

			return datetime;
		}
};



class String
{
	public:

		string zeropad(ULONGLONG n)
		{
			return (n < 10) ? "0" + to_string(n) : to_string(n);
		}
};



class OperatingSystem
{
	public:

		void LastBootUpTime_Detail()
		{
			ULONGLONG tick{ GetTickCount64() / 1000 };

			ULONGLONG seconds{ tick % 60 };
			ULONGLONG minutes{ (tick / 60) % 60 };
			ULONGLONG hours{ (tick / 3600) % 24 };
			ULONGLONG days{ (tick / 86400) };

			String s;

			cout << "The last time the system was started up was\n" << endl;

			cout << "\tDays    : " << s.zeropad(days) << endl;
			cout << "\tHours   : " << s.zeropad(hours) << endl;
			cout << "\tMinutes : " << s.zeropad(minutes) << endl;
			cout << "\tSeconds : " << s.zeropad(seconds) << endl;
			cout << endl;
		}



		string LastBootUpTime_DateTime()
		{
			/*
				Powershell : 
				
				[Management.ManagementDateTimeConverter]::ToDateTime((Get-WmiObject Win32_OperatingSystem).LastBootUpTime) | Select -ExpandProperty TimeOfDay | FT Days, Hours, Minutes, Seconds, Milliseconds
			*/

			/*
			chrono::system_clock::time_point now { chrono::system_clock::now() };
			time_t time { chrono::system_clock::to_time_t(now - chrono::milliseconds(GetTickCount64())) };
			*/

			time_t time { chrono::system_clock::to_time_t(chrono::system_clock::now() - chrono::milliseconds(GetTickCount64())) };

			DateTime dt;

			return dt.display(time);
		}



		void ComputerName()
		{
			TCHAR szComputer[256];
			DWORD dwBufferSize{ 512 };

			GetComputerName(szComputer, &dwBufferSize);

			wcout << szComputer << endl;
		}

};


int main()
{
	OperatingSystem os;

	os.LastBootUpTime_Detail();
	cout << "LastBootUpTime_DateTime : " << os.LastBootUpTime_DateTime() << "\n" << endl;

	//os.ComputerName();

	system("pause");
	return 0;
}